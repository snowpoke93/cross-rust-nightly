# AMD64->ARM64 Cross-compilation for Rust
Features:
- Allows for cross-compilation of Rust programs 
- Convenient for CI: Lets you persist all artifacts by binding the `/artifacts` folder
- You can use any Rust toolchain by setting a `rust-toolchain.toml` file for your project and also persisting the `/artifacts/toolchains` folder.
    The container will load the new toolchain during its first run and subsequently load it from the persisted folder.


## How to Use
You can run this command to compile the project in `~/my-project-root`:
```bash
mkdir -p ~/my-artifacts/{index,cache,git-db,target,toolchains}
docker run -v ~/my-project-root:/root -v ~/my-artifacts:/artifacts snowpoke93/cross-rust-arm64
```

## Artifacts
The artifact folder has the following structure:

|Location|Symlinked Folder|
|---|---|
|`/artifacts/index`| `$CARGO_HOME/registry/index`|
|`/artifacts/cache`| `$CARGO_HOME/registry/cache`|
|`/artifacts/git-db`| `$CARGO_HOME/git/db`|
|`/artifacts/target`| `$CARGO_TARGET_DIR`|
|`/artifacts/toolchains`| `$RUSTUP_HOME/toolchains`|

## License
This project is licensed under both the Apache 2.0 and the MIT open source license.
