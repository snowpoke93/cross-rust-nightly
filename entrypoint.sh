#!/bin/bash

# if a project defines its own toolchain, we have to switch back to ARM64 target before running
set -e
rustup target add aarch64-unknown-linux-gnu
exec "$@"
