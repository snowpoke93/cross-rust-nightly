FROM rust:slim
# term expansion doesn't exist in `sh`, so we switch to `bash`
SHELL ["/bin/bash","-c"]

# install basic software and libraries that might be used during build(like buildpack-deps)
RUN dpkg --add-architecture arm64 && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    netbase \
    wget \
    gnupg \
    dirmngr \
    git \
    openssh-client \
    procps \
    build-essential \
    libc6-arm64-cross \
    libc6-dev-arm64-cross \
    gcc-aarch64-linux-gnu \ 
    libssl-dev libdbus-1-dev \
    libbz2-dev:arm64 \
    libc6-dev:arm64 \
    libcurl4-openssl-dev:arm64 \
    libdb-dev:arm64 \
    libdbus-1-dev:arm64 \
    libevent-dev:arm64 \
    libffi-dev:arm64 \
    libgdbm-dev:arm64 \
    libglib2.0-dev:arm64 \
    libgmp-dev:arm64 \
    libjpeg-dev:arm64 \
    libkrb5-dev:arm64 \
    liblzma-dev:arm64 \
    libmagickcore-dev:arm64 \
    libmagickwand-dev:arm64 \
    libmaxminddb-dev:arm64 \
    libncurses5-dev:arm64 \
    libncursesw5-dev:arm64 \
    libpng-dev:arm64 \
    libpq-dev:arm64 \
    libreadline-dev:arm64 \
    libsqlite3-dev:arm64 \
    libssl-dev:arm64 \
    libwebp-dev:arm64 \
    libxml2-dev:arm64 \
    libxslt-dev:arm64 \
    libyaml-dev:arm64 \
    zlib1g-dev:arm64 \
    default-libmysqlclient-dev:arm64; \
    rm -rf /var/lib/apt/lists/*;

RUN rustup target add aarch64-unknown-linux-gnu

ENV CARGO_HOME=/cargo \
    RUSTUP_HOME=/usr/local/rustup \
    PATH=/cargo/bin:$PATH \
    CARGO_TARGET_DIR=/target \
    USER=root \
    PKG_CONFIG_PATH=/usr/lib/arm-linux-aarch64/pkgconfig \
    PKG_CONFIG_ALLOW_CROSS=true \
    CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER=aarch64-linux-gnu-gcc

# we link artifact folders relevant for CI into one folder: 
# https://doc.rust-lang.org/cargo/guide/cargo-home.html#caching-the-cargo-home-in-ci
# since different toolchains might be loaded in on the fly, we also allow for binding of toolchains
RUN mkdir -p /artifacts/{index,cache,git-db,target,toolchains} /cargo && \
    mkdir -p $CARGO_HOME/registry $CARGO_HOME/git $RUSTUP_HOME/toolchains && \
    ln -s /artifacts/index  $CARGO_HOME/registry/index && \
    ln -s /artifacts/cache $CARGO_HOME/registry/cache && \
    ln -s /artifacts/git-db $CARGO_HOME/git/db && \
    ln -s /artifacts/target $CARGO_TARGET_DIR && \
    ln -s /artifacts/toolchains $RUSTUP_HOME/toolchains

# if a project defines its own toolchain, we have to load the arm64 target for that particular toolchain first
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

WORKDIR /root

ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["cargo", "build", "--target",  "aarch64-unknown-linux-gnu"]
